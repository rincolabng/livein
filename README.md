LiveIn - GovHack 2016 Auckland project by Team Rinco
========

[Project page](http://2016.hackerspace.govhack.org/content/livein)

[Live website](http://livein.rincolab.com/)

[Video](https://www.youtube.com/watch?v=nTiALymxUZs)