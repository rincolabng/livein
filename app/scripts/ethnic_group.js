$(() => {
  $(function () {
    $('#ethnic-group-chart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'Source: Statistics New Zealand (http://nzdotstat.stats.govt.nz/wbos/Index.aspx?DataSetCode=TABLECODE8020)'
        },
        xAxis: {
            categories: ['2001', '2006', '2013']
        },
        yAxis: {
            title: {
                text: 'Number'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Canterbury',
            data: [8466, 12585, 12936]
        }]
    });
});
})