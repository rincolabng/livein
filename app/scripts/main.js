$(() => {
  $('#more-options-toggler').click((event => {
    event.preventDefault()
    $('#more-options').toggle('fast')
  }))
  
  $('#questionnaire-form').submit((event => {
    event.preventDefault()
    $('#answer').toggle('slow')
  }))
})
