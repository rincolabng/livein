$(function () {

    // Prepare demo data
    var data = [
        {
            "hc-key": "nz-au",
            "value": 0
        },
        {
            "hc-key": "nz-ma",
            "value": 0
        },
        {
            "hc-key": "nz-so",
            "value": 0
        },
        {
            "hc-key": "nz-wk",
            "value": 0
        },
        {
            "hc-key": "nz-wg",
            "value": 0
        },
        {
            "hc-key": "nz-4680",
            "value": 0
        },
        {
            "hc-key": "nz-6943",
            "value": 0
        },
        {
            "hc-key": "nz-6947",
            "value": 0
        },
        {
            "hc-key": "nz-ca",
            "value": 18
        },
        {
            "hc-key": "nz-ot",
            "value": 0
        },
        {
            "hc-key": "nz-mw",
            "value": 0
        },
        {
            "hc-key": "nz-gi",
            "value": 0
        },
        {
            "hc-key": "nz-hb",
            "value": 0
        },
        {
            "hc-key": "nz-bp",
            "value": 0
        },
        {
            "hc-key": "nz-3315",
            "value": 0
        },
        {
            "hc-key": "nz-3316",
            "value": 0
        },
        {
            "hc-key": "nz-no",
            "value": 0
        },
        {
            "hc-key": "nz-tk",
            "value": 0
        },
        {
            "hc-key": "nz-wc",
            "value": 0
        }
    ];

    // Initiate the chart
    $('#map').highcharts('Map', {
        mapNavigation: {
            enabled: false
        },

        title: {
            text: ''
        },

        colorAxis: {
            min: 0
        },

        series : [{
            data : data,
            mapData: Highcharts.maps['countries/nz/nz-all'],
            joinBy: 'hc-key',
            name: 'Random data',
            states: {
                hover: {
                    color: '#BADA55'
                }
            },
            dataLabels: {
                enabled: true,
                format: '{point.name}'
            }
        }]
    });
});
