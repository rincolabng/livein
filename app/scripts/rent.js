$(function () {
    $('#rent-chart').highcharts({
        title: {
            text: '',
            x: -20
        },
        subtitle: {
            text: 'Source: Ministry of Business Innovation and Employment (https://data.govt.nz/dataset/show/4731)',
            x: -20
        },
        xAxis: {
            categories: ['1993', '1994', '1995', '1996', '1997', '1998',
                '1999', '2000', '2001', '2002', '2003', '2004',
                '2005', '2006', '2007', '2008', '2009', '2010',
                '2011', '2012', '2013', '2014', '2015', '2016']
        },
        yAxis: {
            title: {
                text: 'Rent'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        tooltip: {
            valuePrefix: '$'
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Canterbury',
            data: [147, 153, 162, 169, 179, 180,
            175, 178, 178, 188, 208, 230, 240, 259,
            263, 287, 288, 290, 303, 319, 354, 393,
            412, 399]
        }]
    });
});
