$(() => {
  $(function () {
    $('#population-chart').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'Source: Statistics New Zealand (http://nzdotstat.stats.govt.nz/wbos/Index.aspx?DataSetCode=TABLECODE8001)'
        },
        xAxis: {
            categories: ['1996', '2001', '2006', '2013']
        },
        yAxis: {
            title: {
                text: 'Number'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name: 'Canterbury',
            data: [468039, 481431, 521832, 539436]
        }]
    });
});
})