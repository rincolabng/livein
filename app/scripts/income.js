$(() => {
  $(function () {
    $('#income-chart').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: 'Source: Statistics New Zealand (http://nzdotstat.stats.govt.nz/wbos/Index.aspx?DataSetCode=TABLECODE8120)'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: 'Group',
            colorByPoint: true,
            data: [{
                name: '$5,000 or Less',
                y: 45
            }, {
                name: '$5,001 - $10,000',
                y: 57
            }, {
                name: '$10,001 - $20,000',
                y: 177
            }, {
                name: '$20,001 - $30,000',
                y: 192
            }, {
                name: '$30,001 - $50,000',
                y: 822
            }, {
                name: '$50,001 or More',
                y: 3069
            }, {
                name: 'Not stated',
                y: 21
            }]
        }]
    });
  });
})
