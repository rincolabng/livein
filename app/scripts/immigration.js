$(function () {
    $('#immigration-chart').highcharts({
        title: {
            text: '',
            x: -20
        },
        subtitle: {
            text: 'Source: Ministry of Business Innovation and Employment (https://data.govt.nz/dataset/show/5595)',
            x: -20
        },
        xAxis: {
            categories: ['2007/08', '2008/09', '2009/10', '2010/11',
              '2011/12', '2012/13', '2013/14', '2014/15', '2015/16'
            ]
        },
        yAxis: {
            title: {
                text: 'Number'
            },
            plotLines: [{
                value: 0,
                width: 1,
                color: '#808080'
            }]
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            borderWidth: 0
        },
        series: [{
            name: 'Canterbury',
            data: [15, 38, 38, 28, 50, 61, 92, 79, 74]
        }]
    });
});