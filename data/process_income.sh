#!/usr/bin/env bash

node raw_data/income/process.js --src=raw_data/income/income.csv --dest=income_by_region-ict.json --occupation="ICT professionals"
