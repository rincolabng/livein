#!/usr/bin/env bash

node raw_data/population/process.js --src=raw_data/population/region.csv --dest=population_by_region.json

node raw_data/population/process.js --src=raw_data/population/sub-region.csv --dest=population_by_sub-region.json