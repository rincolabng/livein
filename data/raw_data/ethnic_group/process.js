'use strict';

const parse = require('csv-parse');
const fs = require('fs');
const path = require('path');
const map = require('lodash.map');
const filter = require('lodash.filter');
const groupBy = require('lodash.groupby');
const mapValues = require('lodash.mapvalues');
const pick = require('lodash.pick');
const argv = require('minimist')(process.argv.slice(2));
const ethnicGroup = argv.ethnic_group;

parse(fs.readFileSync(argv.src), { columns: true }, (err, output) => {
  if (err) {
    console.error(err);
  } else {
    let json = mapValues(groupBy(filter(output, ['Ethnic group', ethnicGroup]), 'Area'), (items) => map(items, (item) => pick(item, ['Year', 'Value'])));
    fs.writeFileSync(argv.dest, JSON.stringify(json, null, 2));
  }
});