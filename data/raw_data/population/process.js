'use strict';

const parse = require('csv-parse');
const fs = require('fs');
const path = require('path');
const map = require('lodash.map');
const groupBy = require('lodash.groupby');
const mapValues = require('lodash.mapvalues');
const pick = require('lodash.pick');
const argv = require('minimist')(process.argv.slice(2));

parse(fs.readFileSync(argv.src), { columns: true }, (err, output) => {
  if (err) {
    console.error(err);
  } else {
    let json = mapValues(groupBy(output, 'Area'), (items) => map(items, (item) => pick(item, ['Year', 'Value'])));
    fs.writeFileSync(argv.dest, JSON.stringify(json, null, 2));
  }
});