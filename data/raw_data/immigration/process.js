'use strict';

const parse = require('csv-parse');
const fs = require('fs');
const path = require('path');
const map = require('lodash.map');
const filter = require('lodash.filter');
const groupBy = require('lodash.groupby');
const countBy = require('lodash.countby');
const mapValues = require('lodash.mapvalues');
const pick = require('lodash.pick');
const argv = require('minimist')(process.argv.slice(2));
const occupation = argv.occupation;
const region = argv.region;

parse(fs.readFileSync(argv.src), { columns: true }, (err, output) => {
  if (err) {
    console.error(err);
  } else {
    let json = countBy(filter(output, {
      'Standard Submajor Group': occupation,
      'Region': region,
      'Decision Type': 'Approved'
    }), 'Financial Year Decided');
    fs.writeFileSync(argv.dest, JSON.stringify(json, null, 2));
  }
});